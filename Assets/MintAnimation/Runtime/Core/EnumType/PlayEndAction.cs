﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Game
{
	public enum PlayEndAction
	{
		None,
		Disable,
		Destory,
		DestoryAnimation
	}
}
