## [0.1.5]-2019-7-2
- Add Bezier in Position Component
- Add Played Default Action
- Add animation in Transform
- Update animation
## [0.1.3]-2019-6-3
- Add "Rotation" animation component (with Euler)
## [0.1.2]-2019-5-19
- Add "Canvas Alpha" animation component
## [0.1.0]-2019-5-18
- Frist version
- Support animation component: Scale, Position, Color
- Support custom animation